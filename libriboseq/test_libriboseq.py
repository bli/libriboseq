import unittest
from itertools import chain, filterfalse, product
import libriboseq


codon_goodness = libriboseq.load_codon_goodness("data/codon_goodness.tsv", codon_col=0, goodness_col=1)
codons = ["".join(triplet) for triplet in product("ACGT", "ACGT", "ACGT")]
good_codons = list(filter(codon_goodness, codons))
bad_codons = list(filterfalse(codon_goodness, codons))


class TestCodonGoodness(unittest.TestCase):
    def test_load_codon_goodness(self):
        codon_goodness = libriboseq.load_codon_goodness("data/codon_goodness.tsv", codon_col=0, goodness_col=1)
        self.assertTrue(codon_goodness("CCA"), "CCA should be good.")
        self.assertTrue(codon_goodness("AGC"), "AGC should be good.")
        self.assertFalse(codon_goodness("TTG"), "TTG should be bad.")
        self.assertFalse(codon_goodness("CGG"), "CGG should be bad.")
        # self.assertTrue(codon_goodness("ZZZ"))
        self.assertTrue(codon_goodness("ATG"), "ATG should be good.")
        self.assertTrue(codon_goodness("TGG"), "TGG should be good.")

A_START = libriboseq.A_SITE.start
P_START = libriboseq.P_SITE.start
A_STOP = libriboseq.A_SITE.stop
P_STOP = libriboseq.P_SITE.stop
test_reads = {
    "RPF29": {
        True: [29 * "N"],
        False: [30 * "N"]
    },
    "RPF29badA": {
        True: [
            A_START * "N" + cod + (29 - A_STOP) * "N"
            for cod in bad_codons
        ],
        False: [
            A_START * "N" + cod + (29 - A_STOP) * "N"
            for cod in good_codons
        ]
    },
    "RPF29goodA": {
        True: [
            A_START * "N" + cod + (29 - A_STOP) * "N"
            for cod in good_codons
        ],
        False: [
            A_START * "N" + cod + (29 - A_STOP) * "N"
            for cod in bad_codons
        ]
    },
    "RPF29badP": {
        True: [
            P_START * "N" + cod + (29 - P_STOP) * "N"
            for cod in bad_codons
        ],
        False: [
            P_START * "N" + cod + (29 - P_STOP) * "N"
            for cod in good_codons
        ]
    },
    "RPF29goodP": {
        True: [
            P_START * "N" + cod + (29 - P_STOP) * "N"
            for cod in good_codons
        ],
        False: [
            P_START * "N" + cod + (29 - P_STOP) * "N"
            for cod in bad_codons
        ]
    },
    "RPF29badAbadP": {
        True: [
            P_START * "N" + cod1 + (A_START - P_STOP) * "N" + cod2 + (29 - A_STOP) * "N"
            for (cod1, cod2) in product(bad_codons, bad_codons)
        ],
        False: [
            P_START * "N" + cod1 + (A_START - P_STOP) * "N" + cod2 + (29 - A_STOP) * "N"
            for (cod1, cod2) in chain(
                product(bad_codons, good_codons),
                product(good_codons, bad_codons),
                product(good_codons, good_codons))
        ]
    },
    "RPF29badAgoodP": {
        True: [
            P_START * "N" + cod1 + (A_START - P_STOP) * "N" + cod2 + (29 - A_STOP) * "N"
            for (cod1, cod2) in product(good_codons, bad_codons)
        ],
        False: [
            P_START * "N" + cod1 + (A_START - P_STOP) * "N" + cod2 + (29 - A_STOP) * "N"
            for (cod1, cod2) in chain(
                product(bad_codons, good_codons),
                product(bad_codons, bad_codons),
                product(good_codons, good_codons))
        ]
    },
    "RPF29goodAbadP": {
        True: [
            P_START * "N" + cod1 + (A_START - P_STOP) * "N" + cod2 + (29 - A_STOP) * "N"
            for (cod1, cod2) in product(bad_codons, good_codons)
        ],
        False: [
            P_START * "N" + cod1 + (A_START - P_STOP) * "N" + cod2 + (29 - A_STOP) * "N"
            for (cod1, cod2) in chain(
                product(bad_codons, bad_codons),
                product(good_codons, bad_codons),
                product(good_codons, good_codons))
        ]
    },
    "RPF29goodAgoodP": {
        True: [
            P_START * "N" + cod1 + (A_START - P_STOP) * "N" + cod2 + (29 - A_STOP) * "N"
            for (cod1, cod2) in product(good_codons, good_codons)
        ],
        False: [
            P_START * "N" + cod1 + (A_START - P_STOP) * "N" + cod2 + (29 - A_STOP) * "N"
            for (cod1, cod2) in chain(
                product(bad_codons, good_codons),
                product(good_codons, bad_codons),
                product(bad_codons, bad_codons))
        ]
    }
}


class TestRPFClassification(unittest.TestCase):
    def setUp(self):
        self.codon_goodness = libriboseq.load_codon_goodness(
            "data/codon_goodness.tsv", codon_col=0, goodness_col=1)
    def test_isrpf29_true(self):
        self.assertTrue(libriboseq.is_rpf29(29), "29 should be the correct size.")
    def test_isrpf29_false(self):
        self.assertFalse(libriboseq.is_rpf29(30), "30 should be a bad size.")
    def test_other_good(self):
        for rpf_subtype in libriboseq.RPF_SUBTYPES:
            fq_filter = libriboseq.make_fq_filter(
                rpf_subtype, self.codon_goodness)
            for test_read in test_reads[rpf_subtype][True]:
                self.assertTrue(fq_filter(("", test_read, "")), f"{test_read} should be {rpf_subtype}")
    def test_other_bad(self):
        for rpf_subtype in libriboseq.RPF_SUBTYPES:
            fq_filter = libriboseq.make_fq_filter(
                rpf_subtype, self.codon_goodness)
            for test_read in test_reads[rpf_subtype][False]:
                self.assertFalse(fq_filter(("", test_read, "")), f"{test_read} should not be {rpf_subtype}")


if __name__ == '__main__':
    unittest.main()
