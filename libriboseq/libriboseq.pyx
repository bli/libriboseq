#!/usr/bin/env python3
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# cython: language_level=3
"""
This module provides things used Ribosome protected fragments annotation.
"""

# http://stackoverflow.com/a/23575771/1878788
# Also works by setting language_level as comment
# at the top of the file.
#from __future__ import print_function
#import warnings
#from sys import stderr
from collections import defaultdict
#from cytoolz import merge_with, frequencies
from itertools import repeat
#from itertools import chain
#from operator import add
#from functools import reduce
#from pysam import AlignedSegment


# cdef int BAM_FREVERSE = 16


# Copied from libsmallrna
# Would need to copy creverse_complement too
# cdef object cget_read_info(object ali):
#     """Extracts information from AlignedSegment *ali*."""
#     if (ali.flag & BAM_FREVERSE) != 0:
#         return (
#             ali.query_name,
#             creverse_complement(ali.query_sequence),
#             # ali.query_qualities[::-1],
#             ali.qual[::-1],
#             ali.query_length,
#             ali.reference_name,
#             # five prime pos:
#             ali.reference_end - 1,
#             ali.reference_start,
#             ali.reference_end,
#             "-")
#     else:
#         return (
#             ali.query_name,
#             ali.query_sequence,
#             # ali.query_qualities,
#             ali.qual,
#             ali.query_length,
#             ali.reference_name,
#             # five prime pos:
#             ali.reference_start,
#             ali.reference_start,
#             ali.reference_end,
#             "+")


# def get_read_info(ali):
#     """Extracts information from AlignedSegment *ali*."""
#     return cget_read_info(ali)


# https://stackoverflow.com/a/14182799/1878788
# Define a new type for a function-type that accepts
# a string, returning a boolean.
# ctypedef bint (*str2bool_type)(str)


# TODO: Add a `header` bool argument?
def load_codon_goodness(str table_path, int codon_col=-1, int goodness_col=-1):
    """
    Create a function telling whether a codon is considered "good", based on
    tabular data in file *table_path*.

    *table_path* should be the path to a tab-separated file with one header
    line with at least one "codon" column and one "good_or_bad" column.
    Alternatively, the (0-based) index of those columns can be specified
    using the *codon_col* and *goodness_col* arguments.

    Default is to consider a codon "good" (for instance, amino-acids having only
    one possible codon cannot have a "bad" codon).
    """
    # https://stackoverflow.com/q/15292687/1878788
    # codon_goodness = defaultdict(repeat(True).__next__)
    # if table_path is not None:
    #     with open(table_path) as fh:
    #         # Parse header line to find columns of interest
    #         headers = fh.readline().strip().split()
    #         if codon_col == -1:
    #             codon_col = headers.index("codon")
    #         if goodness_col == -1:
    #             goodness_col = headers.index("good_or_bad")
    #         # Adding non-default entries in the dictionary
    #         for line in fh:
    #             fields = line.strip().split()
    #             if fields[goodness_col] == "bad":
    #                 codon_goodness[fields[codon_col].upper()] = False
    #             else:
    #                 codon_goodness[fields[codon_col].upper()] = True
    # # The get method of the dict will be a codon -> bool function
    # return codon_goodness.get
    bad_codons = set()
    if table_path is not None:
        with open(table_path) as fh:
            # Parse header line to find columns of interest
            headers = fh.readline().strip().split()
            if codon_col == -1:
                codon_col = headers.index("codon")
            if goodness_col == -1:
                goodness_col = headers.index("good_or_bad")
            for line in fh:
                fields = line.strip().split()
                if fields[goodness_col] == "bad":
                    bad_codons.add(fields[codon_col].upper())
    def codon_goodness(codon):
        return codon not in bad_codons
    return codon_goodness


RPF_SUBTYPES = [
        "RPF29",
        "RPF29badA", "RPF29goodA", "RPF29badP", "RPF29goodP",
        "RPF29badAbadP", "RPF29badAgoodP", "RPF29goodAbadP", "RPF29goodAgoodP"]


cdef bint cread_is_29nt(size_t read_len):
    return read_len == 29


def is_rpf29(int read_len):
    """Return True if this is a RPF29."""
    return cread_is_29nt(read_len)


A_SITE = slice(15,18)
P_SITE = slice(12,15)


cdef bint crpf_has_good_asite(object isgood, str seq):
    return isgood(seq[A_SITE])


cdef bint crpf_has_good_psite(object isgood, str seq):
    return isgood(seq[P_SITE])


def is_rpf29gooda(object isgood, str seq, int read_len):
    """RPF29goodA: 29 nt, good codon at the A site (positions 16-18)"""
    return cread_is_29nt(read_len) and crpf_has_good_asite(isgood, seq)


def is_rpf29bada(object isgood, str seq, int read_len):
    """RPF29badA: 29 nt, bad codon at the A site (positions 16-18)"""
    return cread_is_29nt(read_len) and not crpf_has_good_asite(isgood, seq)


def is_rpf29goodp(object isgood, str seq, int read_len):
    """RPF29goodP: 29 nt, good codon at the P site (positions 13-15)"""
    return cread_is_29nt(read_len) and crpf_has_good_psite(isgood, seq)


def is_rpf29badp(object isgood, str seq, int read_len):
    """RPF29badP: 29 nt, bad codon at the P site (positions 13-15)"""
    return cread_is_29nt(read_len) and not crpf_has_good_psite(isgood, seq)


def is_rpf29goodagoodp(object isgood, str seq, int read_len):
    """RPF29goodAgoodP: 29 nt, good codon at the A site (positions 16-18) and good codon at the P site (positions 13-15)"""
    return cread_is_29nt(read_len) and crpf_has_good_asite(isgood, seq) and crpf_has_good_psite(isgood, seq)


def is_rpf29goodabadp(object isgood, str seq, int read_len):
    """RPF29goodAbadP: 29 nt, good codon at the A site (positions 16-18) and bad codon at the P site (positions 13-15)"""
    return cread_is_29nt(read_len) and crpf_has_good_asite(isgood, seq) and (not crpf_has_good_psite(isgood, seq))


def is_rpf29badagoodp(object isgood, str seq, int read_len):
    """RPF29badAgoodP: 29 nt, bad codon at the A site (positions 16-18) and good codon at the P site (positions 13-15)"""
    return cread_is_29nt(read_len) and (not crpf_has_good_asite(isgood, seq)) and crpf_has_good_psite(isgood, seq)


def is_rpf29badabadp(object isgood, str seq, int read_len):
    """RPF29badAbadP: 29 nt, bad codon at the A site (positions 16-18) and bad codon at the P site (positions 13-15)"""
    return cread_is_29nt(read_len) and (not crpf_has_good_asite(isgood, seq)) and (not crpf_has_good_psite(isgood, seq))


RPF2IS_FUN = {
    "RPF29": is_rpf29,
    "RPF29badA": is_rpf29bada,
    "RPF29goodA": is_rpf29gooda,
    "RPF29badP": is_rpf29badp,
    "RPF29goodP": is_rpf29goodp,
    "RPF29badAbadP": is_rpf29badabadp,
    "RPF29badAgoodP": is_rpf29badagoodp,
    "RPF29goodAbadP": is_rpf29goodabadp,
    "RPF29goodAgoodP": is_rpf29goodagoodp}


def make_fq_filter(str rpf_type, object isgood):
    if rpf_type == "RPF29":
        def fq_filter(fq_triplet):
            """Reads of length 29 are called RPF29."""
            (_, seq, _) = fq_triplet
            return cread_is_29nt(len(seq))
        setattr(fq_filter, "__doc__", RPF2IS_FUN[rpf_type].__doc__)
    else:
        is_fun = RPF2IS_FUN[rpf_type]
        def fq_filter(fq_triplet):
            (_, seq, _) = fq_triplet
            return is_fun(isgood, seq, len(seq))
        setattr(fq_filter, "__doc__", is_fun.__doc__)
    return fq_filter
